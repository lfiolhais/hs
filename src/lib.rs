#![deny(missing_docs)]

//! Human Shell Library

extern crate nix;
#[macro_use]
extern crate log;
extern crate dirs;

pub mod error;
pub mod instr;
pub mod jobs;
pub mod signal;
