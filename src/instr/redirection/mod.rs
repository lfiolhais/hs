//! Redirection module
//! Mainly used by pipes, and <, > and >>
pub mod chain_item;
pub mod redirect;

use std::collections::HashMap;
use std::os::unix::io::RawFd;

use crate::instr::pipe::close_fd;
use crate::instr::utils::update_quote_state;

type AllFds = (
    Option<RawFd>,       // stdin
    Option<RawFd>,       // stdout
    Option<RawFd>,       // stderr
    HashMap<i32, RawFd>, // others
);

// Constants
/// Available redirection symbols
const REDIRECTION_SYMBOLS: [&str; 3] = [">>", "<", ">"];

/// Close stdin file descriptor if it exists
#[inline]
pub fn close_stdin(stdin: Option<RawFd>) {
    // Check if there is a previous assignment to stdin
    if let Some(old_stdin) = stdin {
        debug!("Closing stdin in redirection: {:#?}", old_stdin);
        // Close previous file
        close_fd(old_stdin);
    }
}

/// Close stdout file descriptor if it exists
#[inline]
pub fn close_stdout(stdout: Option<RawFd>) {
    // Check if there is a previous assignment to stdout
    if let Some(old_stdout) = stdout {
        debug!("Closing stdout in redirection: {:#?}", old_stdout);
        // Close previous file
        close_fd(old_stdout);
    }
}

/// Check if string contains any redirection symbol that isn't inside quotes
#[inline]
pub fn contains_redirection(input: &str) -> bool {
    // Check if character is inside a quote and what is that quote.
    let mut inside_quote: bool = false;
    let mut previous_quote: Option<char> = None;

    // Store previous char to assert if the quote is escaped.
    let mut previous_char: char = '\0';
    let mut previous_previous_char: char = '\0';

    for redir_symbol in &REDIRECTION_SYMBOLS {
        for (_, chr) in input.char_indices() {
            let possible_symbol = format!("{}", chr);

            let _ = update_quote_state(
                &mut inside_quote,
                &mut previous_quote,
                [previous_char, previous_previous_char],
                chr,
            );

            if possible_symbol == *redir_symbol
                && !inside_quote
                && (previous_char != '\\' || previous_previous_char == '\\')
            {
                return true;
            }

            // Store previous character and the one before that
            previous_previous_char = previous_char;
            previous_char = chr;
        }
    }

    false
}
