//! Chain Item Module
//! A chain item contains the redirection symbol, the file to which it redirects
//! to and the file descriptor, when applicable.
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::os::unix::io::{IntoRawFd, RawFd};
use std::str::FromStr;

use super::redirect::Redirect;
use super::{close_stdin, close_stdout};
use crate::instr::pipe::close_fd;

/// A component of the chain
#[derive(Debug, PartialEq, Eq)]
pub struct ChainItem {
    /// Redirection Symbol
    pub symbol: String,
    /// Name of the file to create/open
    pub file: String,
    /// File Descriptor Redirection
    pub fd: Option<i32>,
}

const STDIN: i32 = 0;
const STDOUT: i32 = 1;
const STDERR: i32 = 2;

impl ChainItem {
    /// Create Chain Object
    pub fn new(symbol: String, file: String, fd: Option<i32>) -> Self {
        ChainItem { symbol, file, fd }
    }

    /// Handle a file with the following options
    ///
    /// # Arguments
    /// * `file_options` => options to handle the open file (read, write, truncate, etc);
    /// * `stdin`        => stdin file descriptor;
    /// * `stdout`       => stdout file descriptor;
    /// * `stderr`       => stderr file descriptor;
    /// * `others`       => other file descriptors;
    ///
    /// # Return Value
    /// Nothing on success and error message on error
    pub fn file_handling(
        &self,
        file_options: FileOptions,
        stdin: &mut Option<RawFd>,
        stdout: &mut Option<RawFd>,
        stderr: &mut Option<RawFd>,
        others: &mut HashMap<i32, RawFd>,
    ) -> Result<(), <Redirect as FromStr>::Err> {
        match OpenOptions::new()
            .read(file_options.read)
            .append(file_options.append)
            .write(file_options.write)
            .truncate(file_options.truncate)
            .create(file_options.create)
            .open(&self.file)
        {
            Ok(file) => {
                // If there is an explicit file descriptor provided use it and
                // update the file descriptors.
                if let Some(fd) = self.fd {
                    match fd {
                        // Will only close the file descriptor if there is one,
                        // otherwise nothing happens
                        STDIN => {
                            close_stdin(*stdin);
                            *stdin = Some(file.into_raw_fd());
                        }
                        STDOUT => {
                            close_stdout(*stdout);
                            *stdout = Some(file.into_raw_fd());
                        }
                        STDERR => {
                            close_stderr(*stderr);
                            *stderr = Some(file.into_raw_fd());
                        }
                        _ => {
                            // Other file descriptors are changed here
                            debug!("Creating File => Others: {:#?}", others);
                            close_others(others, fd);
                            others.insert(fd, file.into_raw_fd());
                        }
                    }
                } else if file_options.read {
                    // In this case no file descriptor was explicitly provided
                    // in the instruction and the `<` operator was used. Will
                    // only close the file descriptor if there is one, otherwise
                    // nothing happens.
                    close_stdin(*stdin);
                    *stdin = Some(file.into_raw_fd());
                } else if file_options.write {
                    // In this case no file descriptor was explicitly provided
                    // in the instruction and the `>` or `>>` operators were
                    // used. Will only close the file descriptor if there is
                    // one, otherwise nothing happens.
                    close_stdout(*stdout);
                    *stdout = Some(file.into_raw_fd());
                }
            }
            Err(err) => {
                // Will only close the file descriptors if they exist,
                // otherwise nothing happens.
                close_all_fds(*stdin, *stdout, *stderr, others);
                return Err(format!(
                    "Opening/Creating file ({:#?}{}): {}",
                    self.fd, self.symbol, err
                ));
            }
        };

        Ok(())
    }
}

/// Close stderr file descriptor if it exists
#[inline]
fn close_stderr(stderr: Option<RawFd>) {
    // Check if there is a previous assignment to stdout
    if let Some(old_stderr) = stderr {
        debug!("Closing stdout in redirection: {:#?}", old_stderr);
        // Close previous file
        close_fd(old_stderr);
    }
}

/// Close a specific file descriptor in the `HashMap`
#[inline]
fn close_others(others: &HashMap<i32, RawFd>, fd: i32) {
    // Check if the fd has been assigned before
    if others.contains_key(&fd) {
        // Safe unwrap. We just cehcked for the key's existance.
        close_fd(*others.get(&fd).unwrap());
    }
}

/// Close all file descriptors
#[inline]
fn close_all_fds(
    stdin: Option<RawFd>,
    stdout: Option<RawFd>,
    stderr: Option<RawFd>,
    others: &HashMap<i32, RawFd>,
) {
    close_stdin(stdin);
    close_stdout(stdout);
    close_stderr(stderr);

    // Close all file descriptors in the HashMap
    for key in others.keys() {
        close_fd(*others.get(key).unwrap());
    }
}

/// File options to be used when file handling
#[derive(Clone, Copy)]
pub struct FileOptions {
    read: bool,
    write: bool,
    truncate: bool,
    append: bool,
    create: bool,
}

impl FileOptions {
    /// Set the options for the file to be opened with
    pub fn new(read: bool, write: bool, truncate: bool, append: bool, create: bool) -> Self {
        FileOptions {
            read,
            write,
            truncate,
            append,
            create,
        }
    }
}
