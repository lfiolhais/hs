//! Redirect Module
//! The redirect module handles the sequence of redirections in a given command.
//! The module can open files with the correct permissions (`FileOptions`
//! struct) and parses the redirection string.
use std::collections::HashMap;
use std::os::unix::io::RawFd;
use std::str::FromStr;

use super::chain_item::ChainItem;
use super::chain_item::FileOptions;
use super::contains_redirection;
use super::AllFds;
use super::REDIRECTION_SYMBOLS;
use crate::instr::utils::{split, update_quote_state};

/// The redirection object contains an chain of redirection
#[derive(Default, Debug, PartialEq, Eq)]
pub struct Redirect {
    /// Chain of redirection items
    pub chain: Vec<ChainItem>,
}

impl Redirect {
    /// Create a Redirect Object
    pub fn new() -> Self {
        Redirect { chain: Vec::new() }
    }

    /// Parse the carry chain and return the file descriptors for stdin, stdout
    /// and stderr.
    ///
    /// # Return Value
    /// Tuple, the first element is stdin, the second is stdout, the third is
    /// stderr (0, 1 and 2), and the fourth is all other possible file descriptors.
    pub fn get_fds(&self) -> Result<AllFds, <Redirect as FromStr>::Err> {
        let mut stdout: Option<RawFd> = None;
        let mut stdin: Option<RawFd> = None;
        let mut stderr: Option<RawFd> = None;

        // Store other file descriptors in hash map
        let mut others: HashMap<i32, RawFd> = HashMap::new();

        // Allow pipeline to output or append to file
        for redir in &self.chain {
            debug!("stdout: {:#?}", stdout);
            debug!("stdin: {:#?}", stdin);
            debug!("stderr: {:#?}", stderr);
            debug!("Others: {:#?}", others);
            let file_options = match redir.symbol.as_str() {
                // Create, truncate and open in write mode
                ">" => FileOptions::new(false, true, true, false, true),
                // Open in write mode and append
                ">>" => FileOptions::new(false, true, false, true, false),
                // Open in read mode
                "<" => FileOptions::new(true, false, false, false, false),
                _ => {
                    panic!("This shouldn't happen");
                }
            };

            // Cretate/Open file with file_options and update file descriptors
            match redir.file_handling(
                file_options,
                &mut stdin,
                &mut stdout,
                &mut stderr,
                &mut others,
            ) {
                Ok(_) => {}
                Err(err) => return Err(err),
            };
        }

        Ok((stdin, stdout, stderr, others))
    }
}

////////////////////////////////////////////////////////////////////////////////
//                             Implement Parsing                              //
////////////////////////////////////////////////////////////////////////////////
impl FromStr for Redirect {
    type Err = String;

    /// The redirection chain is evaluated from left to right.
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        // Redirection Vector
        let mut redir: Redirect = Redirect::new();

        // Consume the string literal
        let mut input = input.trim().to_owned();

        // Keep parsing input until the selected redirection symbol is no
        // longer present in the string
        while contains_redirection(&input) {
            // Get first redirect symbol from input. This is a safe unwrap, we
            // are checking for its content first.
            let symbol: String = get_first_redirect_symbol(&input).unwrap();

            debug!("First symbol in input: {:#?}", symbol);

            // Split last command in symbol
            let tmp_input = input.clone();
            debug!(
                "\n\nSplitting in redirection with redirection symbol: {:#?}",
                symbol
            );
            let redir_split: Vec<(String, bool)> = match split(&tmp_input, &symbol, false) {
                Ok(split) => split,
                Err(err) => return Err(err),
            };

            debug!("Redirection Split {:#?}", redir_split);

            // Get file descriptor if it exists and is valid, and the start
            // index of the redirectio chain.
            let (fd, fd_length, start_index): (Option<i32>, usize, usize) =
                match get_fd(&redir_split[0].0) {
                    Ok(fd) => fd,
                    Err(err) => return Err(err),
                };

            // Get file from redirection chain and final index of redirection
            // final index of redirection
            let (file, mut end_index): (String, usize) =
                match get_file(&redir_split[1].0, fd_length, symbol.len()) {
                    Ok(file) => file,
                    Err(err) => return Err(err),
                };

            // Count the number of escaped redirection symbols outside of
            // quotes. The only thing `redir_split[1].1` tells us is that there
            // was at least one redirection symbol de-escaped in the input split
            // and that they were de-escaped outside of quotes.
            if redir_split[1].1 {
                let mut previous_char: char = '\0';
                let mut previous_previous_char: char = '\0';

                // Check if character is inside a quote and what is that quote.
                let mut inside_quote: bool = false;
                let mut previous_quote: Option<char> = None;

                for chr in redir_split[1].0.chars() {
                    let possible_symbol = if symbol.len() == 1 {
                        format!("{}", chr)
                    } else {
                        format!("{}{}", previous_char, chr)
                    };

                    let _ = update_quote_state(
                        &mut inside_quote,
                        &mut previous_quote,
                        [previous_char, previous_previous_char],
                        chr,
                    );

                    if possible_symbol == symbol && !inside_quote {
                        end_index += symbol.len();
                    }

                    // Store previous character and the one before that
                    previous_previous_char = previous_char;
                    previous_char = chr;
                }
            }

            if !file.is_empty() {
                debug!("Start Index: {:#?}", start_index);
                debug!("End Index: {:#?}", end_index);
                debug!("File: {:#?}", file);
                debug!("Symbol: {:#?}", symbol);
                debug!("FD: {:#?}", fd);

                // Remove file and redirection symbol from last command in
                // pipeline. The replace method will create a new String object
                // for each invocation. This isn't performant and I should
                // probably find another way to do this.
                debug!("Input: {:#?}", input.trim());
                debug!("Input Size: {:#?}", input.trim().len());
                let tmp_input = input.clone();
                let (left_side, right_side) = tmp_input.trim().split_at(start_index);
                debug!("Left Side: {:#?}", left_side);
                debug!("Left Side Size: {:#?}", left_side.len());
                // Check for contents in the left side of the input split. We
                // need to check for more than one character because the first
                // split will always contain the redirection symbol.
                if left_side.len() > 1 {
                    return Err(format!(
                        "Failed to parse redirection chain (left_side): {:#?}",
                        left_side
                    ));
                }
                debug!("Right Side: {:#?}", right_side);
                debug!("Right Side Size: {:#?}", right_side.len());
                let (_, right_side) = right_side.split_at(end_index - start_index);

                // To create the new input trim the right side contents.
                let new_input = right_side.trim().to_owned();
                debug!("New Input: {:#?}", new_input);

                // Debug Info
                debug!("File: {:#?}", file);
                debug!("New Last Command: {:#?}", new_input);

                // Add redirection
                redir
                    .chain
                    .push(ChainItem::new(symbol.to_string(), file, fd));

                // Replace input
                input = new_input;
            } else {
                return Err("Missing file in redirection".to_owned());
            }
        }

        // If all redirection symbols have been checked and there is still data
        // in the input then we failed to parse the input or the input is
        // invalid
        if !input.is_empty() {
            Err(format!(
                "Failed to parse redirection chain (end): {:#?}",
                input
            ))
        } else {
            Ok(redir)
        }
    }
}

/// Get the first redirection symbol from the input
///
/// # Arguments
/// * `input` => string to search
///
/// # Return Value
/// The redirection symbol if one was found, None otherwise.
fn get_first_redirect_symbol(input: &str) -> Option<String> {
    let mut maybe_duo = false;

    // Check if character is inside a quote and what is that quote.
    let mut inside_quote: bool = false;
    let mut previous_quote: Option<char> = None;

    // Store previous char to assert if the quote is escaped.
    let mut previous_char: char = '\0';
    let mut previous_previous_char: char = '\0';

    for (i, chr) in input.char_indices() {
        let possible_symbol = format!("{}", chr);

        let _ = update_quote_state(
            &mut inside_quote,
            &mut previous_quote,
            [previous_char, previous_previous_char],
            chr,
        );

        if (REDIRECTION_SYMBOLS.contains(&possible_symbol.as_str())
            && !inside_quote
            && (previous_char != '\\' || previous_previous_char == '\\'))
            || maybe_duo
        {
            let possible_duo_symbol = format!("{}{}", previous_char, chr);
            if REDIRECTION_SYMBOLS.contains(&possible_duo_symbol.as_str())
                && previous_previous_char != '\\'
            {
                return Some(possible_duo_symbol);
            } else if maybe_duo {
                return Some(format!("{}", previous_char));
            } else if i == input.len() - 1 {
                return Some(format!("{}", chr));
            } else {
                maybe_duo = true;
            }
        }

        // Store previous character and the one before that
        previous_previous_char = previous_char;
        previous_char = chr;
    }

    None
}

/// Get File Descriptor and its length from input
///
/// # Arguments
/// * `input` => input to parse
///
/// # Return Value
/// Some file descriptor and its length or error message
fn get_fd(input: &str) -> Result<(Option<i32>, usize, usize), String> {
    if input.is_empty() || input.ends_with(' ') {
        Ok((None, 0, input.len()))
    } else {
        // Get the last argument from the split separated by spaces
        let possible_number: String = match split(input, " ", true) {
            Ok(splits) => {
                let ret: Vec<&(String, bool)> = splits.iter().rev().take(1).collect();
                ret[0].0.to_owned()
            }
            Err(err) => return Err(err),
        };
        match possible_number.parse() {
            Ok(number) => Ok((
                Some(number),
                possible_number.len(),
                input.len() - possible_number.len(),
            )),
            Err(_) => Ok((None, 0, input.len())),
        }
    }
}

/// Get the file from the redirection parsing and the total size of the
/// redirection item
///
/// # Arguments
/// * `input` => input to parse
/// * `fd_length` => number of characters in the file descriptor
/// * `symbol_len` => Number of character in the redirection symbol
///
/// # Return Value
/// File and size of the redirection item or error message
fn get_file(input: &str, fd_length: usize, symbol_len: usize) -> Result<(String, usize), String> {
    // Input to split file from
    let input_to_split = input.trim();

    match split(input_to_split, " ", true) {
        Ok(files) => {
            // Store end index of the redirection parsed.
            let mut end_index: usize;

            // I don't think this is the best way to obtain the number
            // of quotes used in the file. The other option is to change
            // split to return inside the tuple the number of quotes
            // removed. However, I don't find this solution particularly
            // helpful since we are just moving the complexity into the
            // split function which is already pretty complex.
            let n_quotes: usize = match split(input_to_split, " ", false) {
                Ok(quote_files) => quote_files[0].0.len() - files[0].0.len(),
                // If the first split is OK then this split will never fail
                Err(_) => unreachable!(),
            };

            // Get number of spaces removed between initial string and parsed string
            let n_spaces = input.len() - input_to_split.len();

            // The end index is computed as follows: the size of the
            // redirection symbol plus the number of whitespaces between
            // the original input and its trimmed version plus the size
            // of the file parsed plus the length of the file descriptor
            // plus the number of quotes removed.
            end_index = symbol_len + n_spaces + files[0].0.len() + fd_length + n_quotes;

            // If there is a difference between the input and the split
            // then the number of escaped spaces removed must be added.
            if files[0].1 {
                end_index += files[0].0.matches(' ').count();
            }

            Ok((files[0].0.to_owned(), end_index))
        }
        Err(err) => Err(err),
    }
}
