//! Pipe
use std::os::unix::io::RawFd;

use nix::fcntl;
use nix::unistd;

use crate::error::{Error as HsError, Result as HsResult};

/// Implements a pipe
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct Pipe {
    fds: (RawFd, RawFd),
}

impl Pipe {
    pub fn new() -> HsResult<Self> {
        match unistd::pipe2(fcntl::O_CLOEXEC) {
            Ok(pipe) => Ok(Pipe {
                fds: (pipe.0, pipe.1),
            }),
            Err(err) => Err(HsError::from(format!("pipe creation error: {}", err))),
        }
    }

    /// Get the file descriptor from the pipe that represents the stdin
    pub fn get_stdin(self) -> RawFd {
        self.fds.0
    }

    /// Get the file descriptor from the pipe that represents the stdout
    pub fn get_stdout(self) -> RawFd {
        self.fds.1
    }

    /// Close both file descriptors
    ///
    /// # Panic
    /// Panics when the file descriptor fails to close.
    pub fn close_fds(self) {
        close_fd(self.fds.0);
        close_fd(self.fds.1);
    }
}

#[inline]
pub fn close_fd(fd: RawFd) {
    debug!("Closing fd: {:#?}", fd);
    match unistd::close(fd) {
        Ok(_) => (),
        Err(err) => {
            panic!("hs error: failed closing a file descriptor: {}", err);
        }
    }
}
