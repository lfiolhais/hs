/// Split the input when `split_arg` is found.
///
/// A split is done when the `split_arg` isn't escaped and isn't inside quotes.
/// The `split_arg` can be escaped using the `\` character and the escape char
/// can be escaped with the same character. To do so, the input is parsed
/// character by character. At the end of each split escape sequences are
/// removed and quotes.
///
/// An escape sequence is denoted by the split arg trailed by '\'. The index
/// where these characters occur are stored in `char_index_del`. The removal
/// process is done through the `remove` method, which will copy the contents of
/// the string into another string.
///
/// # Arguments
/// * `input` => string to split;
/// * `split_arg` => split argument;
/// * `remove_quotes` => should quotes be removed while splitting
///
/// # Return Value
/// A new list with the input split and escaped sequences removed from the input
/// on Ok and an error message on Err.
pub fn split(
    input: &str,
    split_arg: &str,
    remove_quotes: bool,
) -> Result<Vec<(String, bool)>, String> {
    // Create new input
    let mut new_input: Vec<(String, bool)> = vec![];

    // Escaped Args
    let mut escaped_args: bool;

    // List of character indexes to be deleted
    let mut char_index_del: Vec<usize> = vec![];

    // List of quote indexes to be deleted
    let mut quote_index_del: Vec<usize> = vec![];

    // Store the beginning index and end index of the command before the pipe.
    let mut start_index: usize = 0;
    let mut end_index: usize;

    // Check if character is inside a quote and what is that quote.
    let mut inside_quote: bool = false;
    let mut previous_quote: Option<char> = None;

    // Store previous characters to assert if the quote is escaped and the split
    // arg is escaped.
    let mut previous_chars: Vec<char> = vec!['\0'; split_arg.len() + 1];

    for (i, chr) in input.char_indices() {
        // Update input quote status
        if update_quote_state(
            &mut inside_quote,
            &mut previous_quote,
            [previous_chars[0], previous_chars[1]],
            chr,
        ) {
            quote_index_del.push(i - start_index);
        }

        // Create test string to compare to
        let mut test_string = if split_arg.len() > 1 {
            previous_chars.iter().take(split_arg.len() - 1).collect()
        } else {
            String::with_capacity(split_arg.len())
        };
        test_string.push(chr);

        // Perform the split
        if test_string == split_arg
            && !inside_quote
            && (previous_chars[split_arg.len() - 1] != '\\'
                || previous_chars[split_arg.len()] == '\\')
        {
            // There is nothing to split when split_arg length and the
            // current byte index are equal to the start_index
            if split_arg.len() == start_index && i == start_index {
                return Err("Failed to split command".to_owned());
            }

            // We found the end of the command
            end_index = i - start_index - (split_arg.len() - 1);

            // Split command here
            let (_, cmd) = input.split_at(start_index);
            let (cmd, _) = cmd.split_at(end_index);

            escaped_args = !char_index_del.is_empty();

            if remove_quotes {
                update_quote_index(&mut quote_index_del, &mut char_index_del);
            }

            // Removed escaped split arguments
            let cmd = remove_char_at_index(cmd, &mut char_index_del);
            // Remove quotes
            let cmd = if remove_quotes {
                remove_char_at_index(&cmd, &mut quote_index_del)
            } else {
                cmd
            };

            // Store parsed command
            new_input.push((cmd, escaped_args));

            // Update start_index
            start_index = i + 1;
        }

        // Store escape character index to delete it later
        // Need to make sure that the escape symbol isn't escaped.
        if test_string == split_arg
            && !inside_quote
            && previous_chars[split_arg.len() - 1] == '\\'
            && previous_chars[split_arg.len()] != '\\'
        {
            char_index_del.push(i - start_index - 1);
        }

        // Update previous characters
        for j in (0..split_arg.len()).rev() {
            previous_chars[j + 1] = previous_chars[j];
        }
        previous_chars[0] = chr;
    }

    // Store command if we aren't inside a quote
    if !inside_quote {
        // Split command here
        let (_, cmd) = input.split_at(start_index);

        debug!("Parsed: {:#?}", cmd);

        escaped_args = !char_index_del.is_empty();

        if remove_quotes {
            update_quote_index(&mut quote_index_del, &mut char_index_del);
        }

        let cmd = remove_char_at_index(cmd, &mut char_index_del);
        let cmd = if remove_quotes {
            remove_char_at_index(&cmd, &mut quote_index_del)
        } else {
            cmd
        };

        // Store parsed command
        new_input.push((cmd, escaped_args));
    } else {
        return Err("couldn't find end of quote".to_owned());
    }

    Ok(new_input)
}

/// Remove Characters at index.
///
/// # Arguments
/// * `cmd` => command to alter;
/// * `char_index_del` => list of index where characters should be deleted;
///
/// # Return Value
/// Updated String
pub fn remove_char_at_index(cmd: &str, char_index_del: &mut Vec<usize>) -> String {
    let mut cmd = cmd.to_owned();
    // Removed escape characters
    for (i, index) in char_index_del.iter().enumerate() {
        cmd.remove(index - i);
    }

    // Clear List
    char_index_del.clear();

    cmd
}

/// Update the indices of each quote to account for the removal of escape sequences
///
/// # Arguments
/// * `quote_index_del` => list of indexes where quotes were found to delete
/// * `char_index_del` => list of indexes where escape sequences were found to delete
pub fn update_quote_index(quote_index_del: &mut Vec<usize>, char_index_del: &mut Vec<usize>) {
    // Update quote_index. Since the escape sequences will be removed
    // first the indexes of the quotes need to be updated accordingly.
    let mut i: usize = 0;
    while i < quote_index_del.len() {
        let num_less_quote_index_del = char_index_del.iter().fold(0, |total, x| {
            if *x < quote_index_del[i] {
                total + 1
            } else {
                total
            }
        });
        quote_index_del[i] -= num_less_quote_index_del;
        quote_index_del[i + 1] -= num_less_quote_index_del;
        i += 2;
    }
}

/// Update the quote status of the parsed input
///
/// # Arguments
/// * `inside_quote` => Boolean for the quote state
/// * `previous_quote` => quote character that starts the quote
/// * `previous_chars` => previous chars to check if the quote is escaped
///
/// # Return Value
/// A boolean to indicate that the quote status was updated
pub fn update_quote_state(
    inside_quote: &mut bool,
    previous_quote: &mut Option<char>,
    previous_chars: [char; 2],
    current_char: char,
) -> bool {
    if (current_char == '\'' || current_char == '"')
        && (previous_chars[0] != '\\' || previous_chars[1] == '\\')
    {
        if *inside_quote {
            // This should be a safe unwrap since inside_quote is set to
            // true alongside the contents of the previous_quote.
            if current_char == previous_quote.unwrap() {
                *inside_quote = false;
                *previous_quote = None;
                return true;
            }
        } else {
            *inside_quote = true;
            *previous_quote = Some(current_char);
            return true;
        }
    }

    false
}
