//! Module to process shell instructions
use super::cmd::command::Command;
use super::pipe::close_fd;
use super::pipe::Pipe;
use super::redirection::redirect::Redirect;
use super::redirection::{close_stdin, close_stdout, contains_redirection};
use super::utils::{remove_char_at_index, split, update_quote_state};
use crate::error::{Error as HsError, Result as HsResult};
use crate::jobs::job::Job;
use crate::jobs::job_handler::JobHandlerActions;
use crate::signal::kill;

use std::collections::HashMap;
use std::os::unix::io::RawFd;
use std::str::FromStr;

use nix::libc::pid_t;
use nix::sys::signal::Signal;
use nix::sys::wait;
use nix::sys::wait::WaitStatus;

/// Instruction is composed by zero or more pipes and zero or more commands.
#[derive(Default, Debug, Clone)]
pub struct Instruction {
    commands: Vec<Command>,
    pipeline: Vec<Pipe>,
    has_redirection: bool,
    no_wait: bool,
    raw_txt: String,
    children: Vec<pid_t>,
    group_pid: Option<pid_t>,
}

impl Instruction {
    /// Creates a new instance of Instuction
    pub fn new() -> Self {
        Instruction {
            commands: Vec::new(),
            pipeline: Vec::new(),
            has_redirection: false,
            no_wait: false,
            raw_txt: String::new(),
            children: Vec::new(),
            group_pid: None,
        }
    }

    /// Clear the contents of the instruction.
    pub fn clear(&mut self) {
        self.commands.clear();
        self.close_pipeline();
        self.pipeline.clear();
    }

    /// Get the children PID's
    pub fn get_children(&self) -> &[i32] {
        &self.children
    }

    /// Get full instruction before parsing
    pub fn get_raw_txt(&self) -> &str {
        &self.raw_txt
    }

    /// Checks if instruction should wait
    pub fn get_no_wait(&self) -> bool {
        self.no_wait
    }

    /// Close the pipeline
    ///
    /// # Panic
    /// Panics when the file descriptor fails to close.
    fn close_pipeline(&self) {
        self.close_pipeline_at_index(0);
    }

    /// Close the pipeline starting at index
    ///
    /// # Arguments
    /// * `index` => loaction of the first pipes to close
    ///
    /// # Panic
    /// Panics when the file descriptor fails to close.
    fn close_pipeline_at_index(&self, index: usize) {
        for index in index..self.pipeline.len() {
            self.pipeline[index].close_fds();
        }
    }

    /// Create Pipeline
    ///
    /// Create a pipeline with length `n_pipes`
    ///
    /// # Arguments
    /// * `n_pipes` => Length of pipeline
    pub fn create_pipeline(&mut self, n_pipes: usize) {
        // Create pipes
        for _ in 0..n_pipes {
            match Pipe::new() {
                Ok(pipe) => {
                    self.pipeline.push(pipe);
                }
                Err(err) => {
                    // Clean struct and return if the pipe fails to be created
                    self.close_pipeline();
                    panic!("Pipe creation error: {}", err);
                }
            };
        }
    }

    /// Close all file descriptors already consumed by commands
    fn cmd_close_all_fds(&self) {
        for cmd in &self.commands {
            cmd.close_all_fds();
        }
    }

    /// Update `no_wait` parameter. When this parameter is set to true, the `wait_for_child` will
    /// not block when waiting for any command in this instruction.
    pub fn set_no_wait(&mut self, no_wait: bool) {
        self.no_wait = no_wait;
    }

    /// Check if the instruction has redirection
    pub fn has_redirection(&self) -> bool {
        self.has_redirection
    }

    /// Check if the instruction has a pipeline
    pub fn has_pipeline(&self) -> bool {
        !self.pipeline.is_empty()
    }

    /// Number of commands in the instruction
    pub fn n_commands(&self) -> usize {
        self.commands.len()
    }

    /// Get the group PID of the instruction
    pub fn get_group_pid(&self) -> Option<pid_t> {
        self.group_pid
    }

    /// Create Commands
    ///
    /// Create the command object to be executed
    /// element | element | element
    /// 0      \_/       \_/      1
    /// There are three possible situations to change the commands fds:
    /// 1st => First element only changes stdout
    /// 2nd => Last element only changes stdin
    /// 3rd => An element changes both stdout and stdin
    ///
    /// # Arguments
    /// * `commands` => commands to be executed
    /// * `redirects` => redirection chain
    fn create_commands(
        &mut self,
        commands: &[String],
        redirects: &[Redirect],
    ) -> Result<(), String> {
        debug!("Commands: {:#?}", commands);
        for (command_number, command) in commands.iter().enumerate() {
            debug!("Command: {:#?}", command);
            let mut cmd: Command = match command.parse() {
                Ok(cmd) => cmd,
                Err(err) => {
                    self.cmd_close_all_fds();

                    // Close the other end of the pipe that wasn't consumed
                    if command_number == self.pipeline.len() {
                        if !self.pipeline.is_empty() {
                            close_fd(self.pipeline[self.pipeline.len() - 1 as usize].get_stdin());
                        }
                    } else if command_number == 0 {
                        close_fd(self.pipeline[0].get_stdout());
                        close_fd(self.pipeline[0].get_stdin());
                    } else {
                        close_fd(self.pipeline[command_number - 1 as usize].get_stdin());
                        close_fd(self.pipeline[command_number as usize].get_stdout());
                        close_fd(self.pipeline[command_number as usize].get_stdin());
                    }

                    // Close the remainder of the pipeline
                    self.close_pipeline_at_index(command_number + 1);

                    return Err(format!("Parsing Error: {}", err));
                }
            };

            let mut stdin: Option<RawFd> = None;
            let mut stdout: Option<RawFd> = None;
            let mut stderr: Option<RawFd> = None;

            // Store other file descriptors in hash map
            let others: HashMap<i32, RawFd>;

            // If we check for the last command first this function can be
            // reused when no pipeline is present.
            if command_number == self.pipeline.len() {
                // 2nd case
                if !self.pipeline.is_empty() {
                    stdin = Some(self.pipeline[self.pipeline.len() - 1 as usize].get_stdin());
                }

                // Translate the redirection into file descriptors
                match redirects[command_number].get_fds() {
                    Ok(fds) => {
                        if let Some(new_stdin) = fds.0 {
                            close_stdin(stdin);
                            stdin = Some(new_stdin);
                        }
                        if let Some(new_stdout) = fds.1 {
                            stdout = Some(new_stdout);
                        };
                        if let Some(new_stderr) = fds.2 {
                            stderr = Some(new_stderr);
                        };

                        // Store other file descriptors
                        others = fds.3;
                    }
                    Err(err) => {
                        self.cmd_close_all_fds();

                        // We don't need to close any pipe because the only
                        // possible pipe possible is in stdin and we are already
                        // closing that fd.
                        if let Some(stdin) = stdin {
                            close_fd(stdin);
                        }

                        return Err(err.to_owned());
                    }
                };
            } else if command_number == 0 {
                // 1st case
                stdout = Some(self.pipeline[0].get_stdout());

                // Translate the redirection into file descriptors
                match redirects[command_number].get_fds() {
                    Ok(fds) => {
                        if let Some(new_stdin) = fds.0 {
                            stdin = Some(new_stdin);
                        }
                        if let Some(new_stdout) = fds.1 {
                            close_stdout(stdout);
                            stdout = Some(new_stdout);
                        };
                        if let Some(new_stderr) = fds.2 {
                            stderr = Some(new_stderr);
                        };

                        // Store other file descriptors
                        others = fds.3;
                    }
                    Err(err) => {
                        self.cmd_close_all_fds();

                        // Will always close stdout this is a safe unwrap
                        // because there will always be a pipe endpoint
                        // connected.
                        close_fd(stdout.unwrap());

                        // Close the other end of the pipe
                        close_fd(self.pipeline[0].get_stdin());

                        // Close the remainder of the pipeline
                        self.close_pipeline_at_index(command_number + 1);

                        return Err(err.to_owned());
                    }
                };
            } else {
                // 3rd case
                stdin = Some(self.pipeline[command_number - 1 as usize].get_stdin());
                stdout = Some(self.pipeline[command_number as usize].get_stdout());

                // Translate the redirection into file descriptors
                match redirects[command_number].get_fds() {
                    Ok(fds) => {
                        if let Some(new_stdin) = fds.0 {
                            close_stdin(stdin);
                            stdin = Some(new_stdin);
                        }
                        if let Some(new_stdout) = fds.1 {
                            close_stdout(stdout);
                            stdout = Some(new_stdout);
                        };
                        if let Some(new_stderr) = fds.2 {
                            stderr = Some(new_stderr);
                        };

                        // Store other file descriptors
                        others = fds.3;
                    }
                    Err(err) => {
                        self.cmd_close_all_fds();

                        close_fd(stdout.unwrap());
                        close_fd(stdin.unwrap());

                        // Close the other end of the current pipe. We don't
                        // need to close the other end of the previous pipe
                        // becasue it was already consumed by the previous
                        // command and was close in the previous self.commands
                        // loop.
                        close_fd(self.pipeline[command_number as usize].get_stdin());

                        // Close the remainder of the pipeline
                        self.close_pipeline_at_index(command_number + 1);
                        return Err(err.to_owned());
                    }
                };
            }

            // Set file descriptors
            cmd.set_fds(stdin, stdout, stderr, others);

            self.commands.push(cmd);
        }

        Ok(())
    }

    /// Wait for a child.
    ///
    /// # Arguments
    /// * `child` => child PIDs to wait for.
    ///
    /// # Panic
    /// Panics when the shell receives a `WaitStatus` which wasn't properly
    /// handled.
    ///
    /// # Return Value
    /// Collected child
    fn wait_on_child(&self, child: pid_t) -> HsResult<WaitResult> {
        let no_hang_children_flag: Option<wait::WaitPidFlag> = if self.no_wait {
            Some(wait::WNOHANG | wait::WUNTRACED)
        } else {
            Some(wait::WUNTRACED)
        };

        // Wait for child
        match wait::waitpid(child, no_hang_children_flag) {
            Ok(wait_status) => match wait_status {
                WaitStatus::Exited(_, exit_code) => {
                    debug!("Child {} exited with status code {}", child, exit_code);
                    Ok(WaitResult::Done)
                }
                WaitStatus::Signaled(_, signal, _) => {
                    debug!("Child {} signaled with signal {:?}", child, signal);

                    match signal {
                        Signal::SIGTERM => Ok(WaitResult::Dead),
                        Signal::SIGINT => Ok(WaitResult::Dead),
                        _ => {
                            panic!(
                                "Received signal other than SIGTERM or SIGINT. Signal: {:#?}",
                                signal
                            );
                        }
                    }
                }
                WaitStatus::Stopped(_pid, signal) => {
                    // Store child in the Job Handler
                    debug!("Child stopped with {:?}", signal);
                    Ok(WaitResult::JobHandlerAction(JobHandlerActions::SendJobBg))
                }
                WaitStatus::StillAlive => {
                    debug!("Child {} is still alive", child);
                    Ok(WaitResult::StillAlive)
                }
                _ => {
                    eprintln!("Stuff happened which wasn't properly handled. Sorry :(");
                    eprintln!("Wait Status received {:#?}", wait_status);
                    eprintln!("If you feel this is a bug please open a ticket on https://gitlab.com/lfiolhais/hs");
                    eprintln!("Panicking");
                    panic!();
                }
            },
            Err(err) => Err(HsError::from(format!("waiting for child: {}", err))),
        }
    }

    /// Execute all commands and store the PID of each child spawned
    pub fn spawn_instruction(&mut self, jobs: &mut [Job]) -> HsResult<()> {
        // Check if the instruction should fork or not.
        let fork = !self.pipeline.is_empty() | self.has_redirection | self.no_wait;

        for (i, cmd) in self.commands.clone().iter().enumerate() {
            // Get first child PID
            self.group_pid = if i != 0 { Some(self.children[0]) } else { None };

            match cmd.exec(fork, self.no_wait, jobs, self.group_pid) {
                Ok(child_pid) => {
                    if child_pid.is_some() {
                        self.children.push(child_pid.unwrap());
                    }

                    if i == 0 && child_pid.is_some() {
                        self.group_pid = Some(child_pid.unwrap());
                    }
                }
                Err(err) => {
                    match self.wait_on_children() {
                        Ok(_) => return Err(err),
                        Err(err) => return Err(err),
                    };
                }
            };
        }

        // Set foreground process to the new children group if it isn't a bg job or a builtin
        // with no fork.
        if !self.children.is_empty()
            && !self.no_wait
            && self.pipeline.is_empty()
            && !self.has_redirection
        {
            unsafe {
                let _ = libc::tcsetpgrp(0, self.children[0]);
            }
        }

        Ok(())
    }

    /// Execute and wait on all commands
    pub fn exec(&mut self, jobs: &mut [Job]) -> HsResult<()> {
        match self.spawn_instruction(jobs) {
            Ok(_) => {}
            Err(err) => {
                return Err(err);
            }
        };

        // Wait for all children
        if !self.children.is_empty() {
            match self.wait_on_children() {
                Ok(_) => {}
                Err(err) => return Err(err),
            };
        }

        // Set foreground process to the shell
        unsafe {
            let _ = libc::tcsetpgrp(0, libc::getpid());
        }

        Ok(())
    }

    /// Wait on all children and close their file descriptors
    ///
    /// # Panic
    /// If there are no children to collect for this instruction this method will fail.
    ///
    /// # Return Value
    /// If the last child PID was collected
    pub fn wait_on_children(&mut self) -> HsResult<WaitResult> {
        let mut dead_children: Vec<usize> = vec![];

        for (id, child) in self.children.iter().enumerate() {
            match self.wait_on_child(*child) {
                Ok(WaitResult::Done) | Ok(WaitResult::Dead) => {
                    // Close fds
                    self.commands[id].close_all_fds();
                    // Add pid to dead children list
                    dead_children.push(id);
                }
                Ok(WaitResult::StillAlive) => {}
                Ok(WaitResult::JobHandlerAction(action)) => {
                    return Ok(WaitResult::JobHandlerAction(action))
                }
                // Other cases in the enum can't be created by wait_on_child
                Ok(_) => {}
                Err(err) => return Err(err),
            };
        }

        // Remove already collected children from the children list, and remove the respective
        // commands
        dead_children.into_iter().rev().for_each(|id| {
            self.children.remove(id);
            self.commands.remove(id);
        });

        if self.children.is_empty() {
            Ok(WaitResult::AllDone)
        } else {
            Ok(WaitResult::NotDone)
        }
    }

    /// Send a signal to all children
    ///
    /// # Arguments
    /// * `signal` => Signal to send
    pub fn kill_children(&mut self, signal: Signal) -> HsResult<()> {
        for child in &self.children {
            debug!("Sending {:#?} to PID {}", signal, *child);
            match kill(*child, signal) {
                Ok(_) => {}
                Err(err) => return Err(HsError::from(format!("{}", err))),
            }
        }

        // Remove no wait flag
        self.no_wait = false;
        Ok(())
    }

    /// Check if there are any children to collect in this instruction
    pub fn has_children(&self) -> bool {
        !self.children.is_empty()
    }
}

/// Result variants for the wait_on_children function
#[derive(Debug, Eq, PartialEq)]
pub enum WaitResult {
    /// Collected all children in instruction
    AllDone,
    /// There are still children to collect in instruction
    NotDone,
    /// Perform an action in the job handler
    JobHandlerAction(JobHandlerActions),
    /// One child has exited
    Done,
    /// Child is still alive
    StillAlive,
    /// Child has been killed by SIGTERM or SIGINT
    Dead,
}

////////////////////////////////////////////////////////////////////////////////
//                             Implement Parsing                              //
////////////////////////////////////////////////////////////////////////////////
use std::error::Error;
use std::fmt;

/// Different Parsing Errors during the instruction parse
#[derive(Debug)]
pub enum InstructionParseError {
    /// No input received
    Empty,
    /// Specific error with message
    Message(String),
}

impl fmt::Display for InstructionParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            InstructionParseError::Empty => write!(f, "input is empty"),
            InstructionParseError::Message(ref err) => err.fmt(f),
        }
    }
}

impl Error for InstructionParseError {
    fn description(&self) -> &str {
        match *self {
            InstructionParseError::Empty => "input is empty",
            InstructionParseError::Message(ref err) => err,
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            InstructionParseError::Empty => None,
            InstructionParseError::Message(_) => None,
        }
    }
}

impl FromStr for Instruction {
    type Err = InstructionParseError;

    /// Parse the input, with redirection and wait option
    ///
    /// # Return Value
    /// The instruction object to execute
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        // Clear all excessive whitespace from the input
        let mut input = input.trim();

        if input.is_empty() {
            return Err(InstructionParseError::Empty);
        }

        // Clone input to store it later
        let input_clone = String::from(input);

        // No Wait Operator
        let no_wait: bool = parse_no_wait(input);
        // Remove no wait symbol
        if no_wait {
            input = input.trim_end_matches('&').trim();
        };

        // Split the input by pipe characters
        let split_input: Vec<(String, bool)> = match split(input, "|", false) {
            Ok(new_input) => new_input,
            Err(err) => return Err(InstructionParseError::Message(err)),
        };

        // Create a vector of text commands
        let mut commands: Vec<String> = vec![];
        // Create redirection chain
        let mut redirection_chain: Vec<Redirect> = vec![];
        // Has redirection
        let mut has_redirection: bool = false;

        for (cmd, _) in split_input {
            // Separate redirection chain from commands and build a list of both.
            if !cmd.is_empty() {
                // Redirection Chain Object. Process the redirection chain
                let (redirects, new_command) = if contains_redirection(&cmd) {
                    has_redirection = true;
                    let split_cmd_redir = match separate_cmd_from_redirection(&cmd) {
                        Ok(split_cmd_redir) => split_cmd_redir,
                        Err(err) => return Err(InstructionParseError::Message(err)),
                    };

                    // Parse Redirection Chain
                    match split_cmd_redir[1].parse::<Redirect>() {
                        Ok(redir) => (redir, split_cmd_redir[0].to_owned()),
                        Err(err) => return Err(InstructionParseError::Message(err.to_owned())),
                    }
                } else {
                    (Redirect::new(), cmd)
                };

                // Push redirection
                redirection_chain.push(redirects);

                // Save Command
                commands.push(new_command);
            } else {
                // If command is empty after all extra whitespace has been
                // removed than there is no command to parse.
                return Err(InstructionParseError::Message(
                    "imbalanced pipeline".to_owned(),
                ));
            }
        }

        // Create Instruction
        let mut instr = Instruction::new();

        // Store a copy of the instruction text
        instr.raw_txt = input_clone;

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // File descriptor opening and closing only starts from this point
        // forward.
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // Create Pipeline
        instr.create_pipeline(commands.len() - 1);

        // Add redirection check
        instr.has_redirection = has_redirection;

        // Add no wait parameter
        instr.no_wait = no_wait;

        // Create Commands and return instruction object
        match instr.create_commands(&commands, &redirection_chain) {
            Ok(_) => Ok(instr),
            Err(err) => Err(InstructionParseError::Message(err)),
        }
    }
}

/// Parse the no wait operator from the input instruction
///
/// For it to be a valid no wait operator the final character must be a & and be
/// preceded by a space. Anything else will be parsed as either an argument to
/// the last command or a file in the redirection chain.
///
/// # Arguments
/// * `input` => text instruction to parse
///
/// # Return Value
/// True or False
fn parse_no_wait(input: &str) -> bool {
    // Check if command has length greater than 2. If it doesn't there can't be
    // a no wait operator.
    if input.len() >= 2 {
        // Take the last two characters of the last command.
        let last_two: Vec<char> = input.chars().rev().take(2).collect();

        last_two[0] == '&' && last_two[1] == ' '
    } else {
        false
    }
}

/// Remove the redirection chain from the command.
/// TODO: Remove escaped redirection symbol sequences
///
/// # Arguments
/// * `cmd` => command to clean
///
/// # Return Value
/// Tuple: the first string is the command without the redirection chain and the
/// the second string is the redirection chain.
fn separate_cmd_from_redirection(cmd: &str) -> Result<Vec<String>, String> {
    // Create new input
    let mut new_input: Vec<String> = Vec::with_capacity(2);

    // Index of the first character belonging to the redirection chain
    let mut start_redirect: usize = 0;

    // Check if character is inside a quote and what is that quote.
    let mut inside_quote: bool = false;
    let mut previous_quote: Option<char> = None;

    // List of character indexes to be deleted
    let mut char_index_del: Vec<usize> = vec![];

    // Store previous char to assert if the quote is escaped.
    let mut previous_char: char = '\0';
    let mut previous_previous_char: char = '\0';

    // Check if the last argument in the last command is a valid argument or a
    // file descriptor. This only applies if the last argument before the
    // redirection symbol isn't a space.
    for (i, chr) in cmd.char_indices() {
        let _ = update_quote_state(
            &mut inside_quote,
            &mut previous_quote,
            [previous_char, previous_previous_char],
            chr,
        );

        if chr == ' ' && !inside_quote && (previous_char != '\\' || previous_previous_char == '\\')
        {
            start_redirect = i;
        } else if contains_redirection(chr.to_string().as_str()) && !inside_quote {
            if previous_char != '\\' || previous_previous_char == '\\' {
                // There must be a better way to write this...

                // Only check for last argument if the difference of indexes between
                // start_redirect and current index is greater than one. This means
                // there isn't a space before the redirection symbol.
                if i - start_redirect > 1 {
                    // Clone last command
                    let mut tmp_last_command = cmd.to_owned().clone();

                    // Split the command at the last space before a redirection
                    // symbol
                    let mut arg_or_fd =
                        tmp_last_command.split_off(start_redirect).trim().to_owned();
                    // Store the final index before the first redirection symbol
                    let mut end_index: usize = 0;

                    // Get the index of the first redirection symbol
                    for (j, chr) in arg_or_fd.char_indices() {
                        if contains_redirection(chr.to_string().as_str())
                            && (previous_char != '\\' || previous_previous_char == '\\')
                        {
                            end_index = j;
                            break;
                        }
                    }

                    // Cut the string until the first redirection symbol is found
                    arg_or_fd.truncate(end_index);

                    // Try to parse the string. If it fails then the string is an
                    // argument to the last command, otherwise it's a valid file
                    // descriptor. If the number is successfully parsed and is
                    // negative then it's an argument to the comamnd.
                    match arg_or_fd.parse::<i32>() {
                        Ok(number) => {
                            if number < 0 {
                                start_redirect = i;
                            }
                        }
                        Err(_) => start_redirect = i,
                    }
                }

                // Break the loop since we found the end point of the last command
                break;
            } else {
                start_redirect = i + 1;
            }
        }

        // Store escape character index to delete it later. Need to make sure
        // that the escape symbol isn't escaped.
        if contains_redirection(chr.to_string().as_str())
            && !inside_quote
            && previous_char == '\\'
            && previous_previous_char != '\\'
        {
            char_index_del.push(i - 1);
        }

        // Store previous character and the one before that
        previous_previous_char = previous_char;
        previous_char = chr;
    }

    if !inside_quote {
        // Split last command in start redirect thus removing the redirection chain
        // from the command
        let (cmd, redir) = cmd.split_at(start_redirect);

        let cmd = remove_char_at_index(cmd, &mut char_index_del);

        // Store parsed command
        new_input.push(cmd);

        // Store parsed command
        new_input.push(redir.to_owned());

        Ok(new_input)
    } else {
        Err("couldn't find end of quote".to_owned())
    }
}

////////////////////////////////////////////////////////////////////////////////
//                             Implement Equality                             //
////////////////////////////////////////////////////////////////////////////////
impl PartialEq for Instruction {
    /// Only used for tests. Everything is compared as one would expect except
    /// for the file descriptors. In this case we only care if there is Some
    /// value stored.
    fn eq(&self, other: &Instruction) -> bool {
        // Check number of commands available in each instruction
        let same_n_commands = self.commands.len() == other.commands.len();

        if !same_n_commands {
            return false;
        }

        // Check for equality inside each command
        // When performing this check fds must be ignored
        let mut commands_equal: bool = true;
        for (cmd1, cmd2) in self.commands.iter().zip(other.commands.iter()) {
            if cmd1 != cmd2 {
                commands_equal = false;
                break;
            }
        }

        // Check for the existance of a pipeline and its length. We don't need
        // to check for the contents of the pipeline because the fds will
        // necessarily be different.
        let same_n_pipes = self.pipeline.len() == other.pipeline.len();

        // Check for the presence of redirection
        let has_redirection = self.has_redirection == other.has_redirection;

        // Check for the presence of a no_wait operator
        let no_wait = self.no_wait == other.no_wait;

        // Perform an AND across all values
        same_n_commands & commands_equal & same_n_pipes & has_redirection & no_wait
    }
}
