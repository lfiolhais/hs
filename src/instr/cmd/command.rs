//! Command
//!
//! Module used to parse and execute commands
use std::collections::HashMap;
use std::env;
use std::ffi::CString;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::io::RawFd;
use std::process;
use std::str::FromStr;

use nix::unistd;
use nix::unistd::ForkResult;

use super::builtins;
use super::builtins::Builtins;
use crate::instr::pipe::close_fd;
use crate::instr::utils::split;
use crate::jobs::job::Job;

use crate::error::{Error as HsError, Result as HsResult};

// Constants
const COMMAND_FAILURE: i32 = 127;
const COMMAND_SUCCESS: i32 = 0;

/// Command Struct
///
/// Stores the name of the command and its arguments
#[derive(Debug, Default, Clone)]
pub struct Command {
    name: Builtins,
    args: Vec<String>,
    stdin: Option<RawFd>,
    stdout: Option<RawFd>,
    stderr: Option<RawFd>,
    others: HashMap<i32, RawFd>,
}

impl Command {
    /// Creates a new instance of Command
    pub fn new() -> Self {
        Command {
            name: Builtins::new(),
            args: Vec::new(),
            stdin: None,
            stdout: None,
            stderr: None,
            others: HashMap::new(),
        }
    }

    /// Set all file descriptors of the process
    ///
    /// # Arguments
    /// * `stdin` => The stdin file descriptor
    /// * `stdout` => The stdout file descriptor
    /// * `stderr` => The stderr file descriptor
    pub fn set_fds(
        &mut self,
        stdin: Option<RawFd>,
        stdout: Option<RawFd>,
        stderr: Option<RawFd>,
        others: HashMap<i32, RawFd>,
    ) {
        self.stdin = stdin;
        self.stdout = stdout;
        self.stderr = stderr;
        self.others = others;
    }

    /// Close all file descriptors used in the command
    pub fn close_all_fds(&self) {
        // Stdin
        if let Some(stdin) = self.stdin {
            debug!("Closing stdin: {:#?}", stdin);
            close_fd(stdin);
        };

        // Stdout
        if let Some(stdout) = self.stdout {
            debug!("Closing stdout: {:#?}", stdout);
            close_fd(stdout);
        };

        // Stderr
        if let Some(stderr) = self.stderr {
            debug!("Closing stderr: {:#?}", stderr);
            close_fd(stderr);
        };

        // Close all other file descriptors
        for key in self.others.keys() {
            close_fd(self.others[key]);
        }
    }

    /// Update all file descriptors in the command
    fn update_fds(&self) {
        // Stdin
        if let Some(stdin) = self.stdin {
            unistd::dup2(stdin, 0).unwrap_or_else(|err| {
                eprintln!("hs error: {}", err);
                process::exit(COMMAND_FAILURE);
            });
        };

        // Stdout
        if let Some(stdout) = self.stdout {
            unistd::dup2(stdout, 1).unwrap_or_else(|err| {
                eprintln!("hs error: {}", err);
                process::exit(COMMAND_FAILURE);
            });
        };

        // Stderr
        if let Some(stderr) = self.stderr {
            unistd::dup2(stderr, 2).unwrap_or_else(|err| {
                eprintln!("hs error: {}", err);
                process::exit(COMMAND_FAILURE);
            });
        };

        // Update others
        for key in self.others.keys() {
            unistd::dup2(self.others[key], *key).unwrap_or_else(|err| {
                eprintln!("hs error: {}", err);
                process::exit(COMMAND_FAILURE);
            });
        }
    }

    /// Fork Process
    ///
    /// Simple fork. The parent waits on a child and when done closes its file
    /// descriptors. The child updates its file descriptors and executes the
    /// given command. If the child fails to execute the command the status code
    /// 127 is returned.
    ///
    /// # Return Value
    /// The child PID.
    fn fork(&self, jobs: &mut [Job], group_pid: Option<i32>, no_wait: bool) -> HsResult<i32> {
        match unistd::fork() {
            Ok(ForkResult::Parent { child }) => {
                // Set child to have their own process group. If the spawned child is the first in
                // the pipeline set PGID to PID. Otherwise set PGID to the PID of the first child
                // in the pipeline.
                let new_pgid = if group_pid.is_some() {
                    group_pid.unwrap()
                } else {
                    child
                };

                unsafe {
                    // Set parent ID group
                    libc::setpgid(child, new_pgid);
                }

                Ok(child)
            }

            Ok(ForkResult::Child) => {
                // Update file descriptors
                self.update_fds();

                // Set foreground process
                let tpgid = if group_pid.is_some() {
                    group_pid.unwrap()
                } else {
                    unsafe { libc::getpid() }
                };

                // Set foreground process to the new children group if it isn't a bg job.
                if !no_wait {
                    unsafe {
                        let _ = libc::tcsetpgrp(0, tpgid);
                    }
                }

                // Don't ignore signals
                unsafe {
                    let _ = libc::signal(libc::SIGINT, libc::SIG_DFL);
                    let _ = libc::signal(libc::SIGTSTP, libc::SIG_DFL);
                    let _ = libc::signal(libc::SIGTTOU, libc::SIG_DFL);
                }

                // This a bunch of repeated code in here that I would like to cleanup.
                // Match Builtins
                match self.name {
                    Builtins::Cd => match builtins::cd(self.args.as_slice()) {
                        Ok(()) => process::exit(COMMAND_SUCCESS),
                        Err(err) => {
                            eprintln!("hs error fork failed: {}", err);
                            process::exit(COMMAND_FAILURE);
                        }
                    },
                    Builtins::Echo => match builtins::echo(self.args.as_slice()) {
                        Ok(()) => process::exit(COMMAND_SUCCESS),
                        Err(err) => {
                            eprintln!("hs error fork failed: {}", err);
                            process::exit(COMMAND_FAILURE);
                        }
                    },
                    Builtins::Export => match builtins::export(self.args.as_slice()) {
                        Ok(()) => process::exit(COMMAND_SUCCESS),
                        Err(err) => {
                            eprintln!("hs error fork failed: {}", err);
                            process::exit(COMMAND_FAILURE);
                        }
                    },
                    Builtins::Jobs => {
                        builtins::jobs(self.args.as_slice(), jobs);
                        process::exit(COMMAND_SUCCESS);
                    }
                    Builtins::Bg => match builtins::bg(self.args.as_slice(), jobs) {
                        Ok(()) => process::exit(COMMAND_SUCCESS),
                        Err(err) => {
                            eprintln!("hs error fork failed: {}", err);
                            process::exit(COMMAND_FAILURE);
                        }
                    },
                    Builtins::Fg => match builtins::fg(self.args.as_slice(), jobs) {
                        Ok(()) => process::exit(COMMAND_SUCCESS),
                        Err(err) => {
                            eprintln!("hs error fork failed: {}", err);
                            process::exit(COMMAND_FAILURE);
                        }
                    },
                    Builtins::Exit => match builtins::exit(self.args.as_slice(), jobs) {
                        Ok(()) => process::exit(COMMAND_SUCCESS),
                        Err(err) => {
                            eprintln!("hs error fork failed: {}", err);
                            process::exit(COMMAND_FAILURE);
                        }
                    },
                    Builtins::None(_) => {
                        // Safe unwrap
                        let name =
                            CString::new(self.name.as_ref().as_bytes()).unwrap_or_else(|err| {
                                eprintln!("hs error: {}", err);
                                process::exit(COMMAND_FAILURE);
                            });

                        // Build new arguments
                        let mut new_args: Vec<CString> = vec![];
                        for arg in &self.args {
                            let new_arg = CString::new(arg.as_bytes()).unwrap_or_else(|err| {
                                eprintln!("hs error: {}", err);
                                process::exit(COMMAND_FAILURE);
                            });
                            new_args.push(new_arg);
                        }
                        // Name of command to execute should be first argument
                        new_args.insert(0, name);

                        // Substitute process
                        match unistd::execvp(&new_args[0], new_args.as_slice()) {
                            // This should never, Never, NEVER happen
                            Ok(_) => unreachable!(),
                            Err(err) => {
                                eprintln!("hs error: {}", err);
                                process::exit(COMMAND_FAILURE);
                            }
                        }
                    }
                }
            }

            Err(err) => Err(HsError::from(format!("Fork Failed: {}", err))),
        }
    }

    /// Match and Execute command
    ///
    /// The execution of the command varies depending on the conditions of the
    /// instruction. If a pipeline or a redirection chain is present every
    /// single command in the pipeline will be forked, even builtins. If no
    /// pipeline or redirection is present the command will be forked if it
    /// isn't a builtin.
    ///
    /// # Arguments
    /// * `fork` => the command to be executed is inside a pipeline
    /// or has a redirection chain
    ///
    /// # Return Value
    /// On success it will return the child PID if applicable and if the parent
    /// process should wait on the child. `-1` is used to represent an invalid
    /// child PID, since a PID is always a natural number.
    pub fn exec(
        &self,
        fork: bool,
        no_wait: bool,
        jobs: &mut [Job],
        group_pid: Option<i32>,
    ) -> HsResult<Option<i32>> {
        // Fork any command if it is in a pipeline, has redirection or is a background job
        if !fork {
            // Match Builtins
            match self.name {
                Builtins::Cd => match builtins::cd(self.args.as_slice()) {
                    Ok(_) => Ok(None),
                    Err(err) => Err(HsError::from(format!("{}", err))),
                },
                Builtins::Echo => match builtins::echo(self.args.as_slice()) {
                    Ok(_) => Ok(None),
                    Err(err) => Err(HsError::from(format!("{}", err))),
                },
                Builtins::Export => match builtins::export(self.args.as_slice()) {
                    Ok(_) => Ok(None),
                    Err(err) => Err(HsError::from(format!("{}", err))),
                },
                Builtins::Jobs => {
                    builtins::jobs(self.args.as_slice(), jobs);
                    Ok(None)
                }
                Builtins::Bg => match builtins::bg(self.args.as_slice(), jobs) {
                    Ok(_) => Ok(None),
                    Err(err) => Err(HsError::from(format!("{}", err))),
                },
                Builtins::Fg => match builtins::fg(self.args.as_slice(), jobs) {
                    Ok(_) => Ok(None),
                    Err(err) => Err(HsError::from(format!("{}", err))),
                },
                Builtins::Exit => match builtins::exit(self.args.as_slice(), jobs) {
                    Ok(_) => Ok(None),
                    Err(err) => Err(HsError::from(format!("{}", err))),
                },
                Builtins::None(_) => match self.fork(jobs, group_pid, no_wait) {
                    Ok(child_pid) => Ok(Some(child_pid)),
                    Err(err) => Err(err),
                },
            }
        } else {
            match self.fork(jobs, group_pid, no_wait) {
                Ok(child_pid) => Ok(Some(child_pid)),
                Err(err) => Err(err),
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//                             Implement Parsing                              //
////////////////////////////////////////////////////////////////////////////////
impl FromStr for Command {
    type Err = String;

    /// Parses the contents of the input to the Command struct
    ///
    /// # Arguments
    /// * `input` => input to process
    ///
    /// # Return Value
    /// On success the Command object.
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut cmd = Command::new();

        let new_input: Vec<(String, bool)> = match split(input, " ", true) {
            Ok(new_input) => {
                let out: Vec<(String, bool)> = new_input
                    .iter()
                    .map(|x| (x.0.to_owned(), x.1))
                    .filter(|x| !x.0.is_empty())
                    .collect();

                if out.is_empty() {
                    return Err("No commands found".to_owned());
                } else {
                    out
                }
            }
            Err(err) => return Err(err),
        };

        debug!("Command after parse: {:#?}", new_input);

        for (i, &(ref arg, _)) in new_input.iter().enumerate() {
            if i == 0 {
                cmd.name = match arg.parse::<Builtins>() {
                    Ok(name) => name,
                    Err(err) => return Err(err),
                };
            } else if arg.starts_with('$') {
                let new_arg = arg[1..].to_owned();

                if !new_arg.contains('\0') {
                    match env::var(&new_arg) {
                        Ok(contents) => cmd.args.push(contents),
                        Err(err) => {
                            return Err(format!("couldn't interpret {}: {}", new_arg, err));
                        }
                    };
                } else {
                    return Err("special argument can't contain null byte".to_owned());
                }
            } else {
                cmd.args.push((*arg).to_owned());
            }
        }

        Ok(cmd)
    }
}

////////////////////////////////////////////////////////////////////////////////
//                             Implement Equality                             //
////////////////////////////////////////////////////////////////////////////////
impl PartialEq for Command {
    /// Only used for tests. Everything is compared as one would expect except
    /// for the file descriptors. In this case we only care if there is Some
    /// value stored.
    fn eq(&self, other: &Command) -> bool {
        // Check for same command name
        let same_cmd_name = self.name == other.name;

        // Check for same arguments
        // First check for argument list length than content.
        let same_n_args = self.args.len() == other.args.len();

        if !same_n_args {
            return false;
        }

        // Check for equality inside each argument
        let mut args_equal: bool = true;
        for (arg1, arg2) in self.args.iter().zip(other.args.iter()) {
            if arg1 != arg2 {
                args_equal = false;
                break;
            }
        }

        // When checking file descriptor the only property that matter is if
        // there is a file descriptor present.
        let has_stdin = self.stdin.is_some() == other.stdin.is_some();
        let has_stdout = self.stdout.is_some() == other.stdout.is_some();
        let has_stderr = self.stderr.is_some() == other.stderr.is_some();

        same_cmd_name & same_n_args & args_equal & has_stdin & has_stdout & has_stderr
    }
}
