//! Shell builtins
use std::convert::AsRef;
use std::ffi::OsStr;
use std::fmt;
use std::str::FromStr;

use crate::jobs::job::Job;

mod bg;
mod cd;
mod echo;
mod exit;
mod export;
mod fg;
mod jobs;

type BuiltinResult = Result<(), Error>;

/// Error type for all Builtins
#[derive(Debug)]
pub enum Error {
    /// Absorb an error coming from the cd builtin
    CdError(self::cd::Error),
    /// Absorb an error coming from the echo builtin
    EchoError(self::echo::Error),
    /// Absorb an error coming from the export builtin
    ExportError(self::export::Error),
    /// Absorb an error coming from the exit builtin
    ExitError(self::exit::Error),
    /// Absorb an error coming from the bg builtin
    BgError(self::bg::Error),
    /// Absorb an error coming from the fg builtin
    FgError(self::fg::Error),
}

impl From<self::cd::Error> for Error {
    fn from(err: self::cd::Error) -> Self {
        Error::CdError(err)
    }
}

impl From<self::echo::Error> for Error {
    fn from(err: self::echo::Error) -> Self {
        Error::EchoError(err)
    }
}

impl From<self::export::Error> for Error {
    fn from(err: self::export::Error) -> Self {
        Error::ExportError(err)
    }
}

impl From<self::exit::Error> for Error {
    fn from(err: self::exit::Error) -> Self {
        Error::ExitError(err)
    }
}

impl From<self::bg::Error> for Error {
    fn from(err: self::bg::Error) -> Self {
        Error::BgError(err)
    }
}

impl From<self::fg::Error> for Error {
    fn from(err: self::fg::Error) -> Self {
        Error::FgError(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::CdError(err) => write!(f, "\ncd error: {}", err),
            Error::EchoError(err) => write!(f, "\necho error: {}", err),
            Error::ExportError(err) => write!(f, "\nexport error: {}", err),
            Error::ExitError(err) => write!(f, "\nexit error: {}", err),
            Error::BgError(err) => write!(f, "\nbg error: {}", err),
            Error::FgError(err) => write!(f, "\nfg error: {}", err),
        }
    }
}

/// Builtins available by the shell
#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Builtins {
    /// Change Directory
    Cd,
    /// Echo
    Echo,
    /// Export,
    Export,
    /// Jobs
    Jobs,
    /// Exit
    Exit,
    /// Send Job to Background
    Bg,
    /// Send Job to Foreground
    Fg,
    /// Fork process and run command
    None(String),
}

impl FromStr for Builtins {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "cd" => Ok(Builtins::Cd),
            "echo" => Ok(Builtins::Echo),
            "export" => Ok(Builtins::Export),
            "jobs" => Ok(Builtins::Jobs),
            "exit" => Ok(Builtins::Exit),
            "bg" => Ok(Builtins::Bg),
            "fg" => Ok(Builtins::Fg),
            _ => {
                if s.is_empty() {
                    Err("Empty command".to_owned())
                } else {
                    Ok(Builtins::None(s.to_owned()))
                }
            }
        }
    }
}

impl Default for Builtins {
    fn default() -> Self {
        Builtins::None("".to_owned())
    }
}

impl AsRef<OsStr> for Builtins {
    fn as_ref(&self) -> &OsStr {
        match *self {
            Builtins::Cd => "cd".as_ref(),
            Builtins::Echo => "echo".as_ref(),
            Builtins::Export => "export".as_ref(),
            Builtins::Jobs => "jobs".as_ref(),
            Builtins::Exit => "exit".as_ref(),
            Builtins::Bg => "bg".as_ref(),
            Builtins::Fg => "fg".as_ref(),
            Builtins::None(ref cmd) => cmd.as_ref(),
        }
    }
}

impl Builtins {
    /// Initialize Builtins
    pub fn new() -> Builtins {
        Builtins::default()
    }
}

/// Wrapper function for the cd builtin
pub fn cd(args: &[String]) -> BuiltinResult {
    match cd::cd(args) {
        Ok(_) => Ok(()),
        Err(err) => Err(Error::CdError(err)),
    }
}

/// Wrapper function for the echo builtin
pub fn echo(args: &[String]) -> BuiltinResult {
    match echo::echo(args) {
        Ok(_) => Ok(()),
        Err(err) => Err(Error::EchoError(err)),
    }
}

/// Wrapper function for the export builtin
pub fn export(args: &[String]) -> BuiltinResult {
    match export::export(args) {
        Ok(_) => Ok(()),
        Err(err) => Err(Error::ExportError(err)),
    }
}

/// Wrapper function for the exit builtin
pub fn exit(args: &[String], jobs: &mut [Job]) -> BuiltinResult {
    match exit::exit(args, jobs) {
        Ok(_) => Ok(()),
        Err(err) => Err(Error::ExitError(err)),
    }
}

/// Wrapper function for the jobs builtin
pub fn jobs(args: &[String], jobs: &[Job]) {
    jobs::jobs(args, jobs);
}

/// Wrapper function for the bg builtin
pub fn bg(args: &[String], jobs: &mut [Job]) -> BuiltinResult {
    match bg::bg(args, jobs) {
        Ok(_) => Ok(()),
        Err(err) => Err(Error::BgError(err)),
    }
}

/// Wrapper function for the fg builtin
pub fn fg(args: &[String], jobs: &mut [Job]) -> BuiltinResult {
    match fg::fg(args, jobs) {
        Ok(_) => Ok(()),
        Err(err) => Err(Error::FgError(err)),
    }
}
