use std::env;
use std::fmt;
use std::io;
use std::path::PathBuf;

/// cd builtin
///
/// Will error out when more than one argument is provided. cd'ing without an
/// argument goes to the users home.
///
/// # Arguments
/// * `args` => arguments to process the cd
pub fn cd(args: &[String]) -> Result<(), Error> {
    let new_dir = if args.len() > 1 {
        // Error if more than one argument is provided
        return Err(Error::TooManyArgs);
    } else if args.is_empty() {
        // If no argument is provided cd to home
        match dirs::home_dir() {
            Some(home) => home,
            None => {
                return Err(Error::HomeIsNotSet);
            }
        }
    } else {
        PathBuf::from(args[0].as_str())
    };

    // Get current directory
    let new_pwd = new_dir.canonicalize()?;

    // Update PWD
    env::set_var("PWD", format!("{}", new_pwd.display()));

    // Change directory
    match env::set_current_dir(new_dir) {
        Ok(()) => Ok(()),
        Err(io_err) => Err(Error::StdIOError(io_err)),
    }
}

/// cd builtin Error type
#[derive(Debug)]
pub enum Error {
    /// The environment variable $HOME is not set
    HomeIsNotSet,
    /// Builtin received too many arguments
    TooManyArgs,
    /// Failed to canonicalize the current directory or failed setting the current directory
    StdIOError(io::Error),
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::StdIOError(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::HomeIsNotSet => write!(f, "$HOME environment variable is not set"),
            Error::TooManyArgs => write!(f, "too many arguments provided"),
            Error::StdIOError(io_err) => write!(f, "{}", io_err),
        }
    }
}
