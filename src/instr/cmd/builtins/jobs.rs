use crate::jobs::job::Job;

/// Print information about all jobs currently in the shell
pub fn jobs(_: &[String], jobs: &[Job]) {
    println!(
        "{:<6}\t {:<20}\t {:<9}\t Group PID",
        "Job Id", "Command", "Status"
    );

    for job in jobs {
        println!("{}", job);
    }
}
