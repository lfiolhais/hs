use std::fmt;
use std::io;
use std::io::Write;

/// echo builtin
///
/// Echos the arguments given.
///
/// # Arguments
/// * `args` => arguments to echo
pub fn echo(args: &[String]) -> Result<(), Error> {
    if !args.is_empty() {
        let (no_new_line, new_args): (bool, &[String]) = if args[0] == "-n" {
            (true, &args[1..])
        } else {
            (false, args)
        };

        for (i, arg) in new_args.iter().enumerate() {
            if i != 0 && i != args.len() {
                print!(" ");
            }
            print!("{}", arg);
        }

        if !no_new_line {
            println!();
        }

        io::stdout().flush()?;
    }

    Ok(())
}

/// echo builtin Error type
#[derive(Debug)]
pub enum Error {
    /// Failed to flush stdout
    StdIOError(io::Error),
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::StdIOError(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::StdIOError(io_err) => write!(f, "{}", io_err),
        }
    }
}
