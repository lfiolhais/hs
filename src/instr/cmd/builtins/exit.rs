use std::fmt;
use std::num;
use std::process;

use crate::jobs::job::Job;

fn kill_all_jobs(jobs: &mut [Job]) {
    jobs.iter_mut().for_each(|job| job.kill_all());
}

/// Exit builtin
///
/// Will kill all jobs in the job handler before exiting the shell.
/// WARNING: Calling exit in a pipeline, redirection or no_wait will only kill the forked process
/// instead of the shell, but will kill all jobs in the job handler.
///
/// # Arguments
/// * `args` => Only the first number will be read
pub fn exit(args: &[String], jobs: &mut [Job]) -> Result<(), Error> {
    if !args.is_empty() {
        if args.len() > 1 {
            return Err(Error::TooManyArgs);
        }
        match args[0].parse::<i32>() {
            Ok(number) => {
                if number < 128 && number > -129 {
                    kill_all_jobs(jobs);
                    process::exit(number);
                } else {
                    Err(Error::InvalidNumber)
                }
            }
            Err(err) => Err(Error::ParseError(err)),
        }
    } else {
        kill_all_jobs(jobs);
        process::exit(0);
    }
}

/// exit builtin Error type
#[derive(Debug)]
pub enum Error {
    /// The provided exit code isn't valid
    InvalidNumber,
    /// Builtin received too many arguments
    TooManyArgs,
    /// Failed to parse exit code
    ParseError(num::ParseIntError),
}

impl From<num::ParseIntError> for Error {
    fn from(err: num::ParseIntError) -> Self {
        Error::ParseError(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::InvalidNumber => write!(f, "exit code must be between -128 and 127"),
            Error::TooManyArgs => write!(f, "too many arguments provided"),
            Error::ParseError(io_err) => write!(f, "{}", io_err),
        }
    }
}
