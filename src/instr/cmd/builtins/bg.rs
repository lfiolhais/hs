use crate::jobs::job::{Job, JobStatus};
use std::fmt;
use std::num;

use nix::sys::signal::Signal;

/// Send a job_id to the background and resume if it is stopped. If no argument is
/// provided the latest job in the handler will be sent to the background.
pub fn bg(args: &[String], jobs: &mut [Job]) -> Result<(), Error> {
    if jobs.is_empty() {
        return Err(Error::NoJobs);
    }

    let sel_job: Option<&mut Job> = {
        if args.is_empty() {
            Some(&mut jobs[jobs.len() - 1])
        } else {
            // Parse arguments
            let provided_id = match args[0].parse::<usize>() {
                Ok(id) => id,
                Err(err) => {
                    return Err(Error::ParseError(err));
                }
            };

            // Check for existance of job id
            let sel_job = jobs.iter_mut().find(|job| job.get_id() == provided_id);

            if sel_job.is_some() {
                sel_job
            } else {
                return Err(Error::NonExistantID);
            }
        }
    };

    let job = sel_job.unwrap();

    job.borrow_instruction_mut()
        .kill_children(Signal::SIGCONT)
        .unwrap_or_else(|err| eprintln!("{}", err));
    job.change_job_status(JobStatus::Running);
    job.borrow_instruction_mut().set_no_wait(true);

    Ok(())
}

/// bg builtin Error type
#[derive(Debug)]
pub enum Error {
    /// Provided ID doesn't exist in the job handler
    NonExistantID,
    /// Provided ID isn't valid
    ParseError(num::ParseIntError),
    /// There are no jobs in the job handler
    NoJobs,
}

impl From<num::ParseIntError> for Error {
    fn from(err: num::ParseIntError) -> Self {
        Error::ParseError(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::NonExistantID => write!(f, "provided job ID doesn't exist"),
            Error::ParseError(err) => write!(f, "{}", err),
            Error::NoJobs => write!(f, "there are no jobs in the job handler"),
        }
    }
}
