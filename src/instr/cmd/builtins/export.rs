use std::env;
use std::fmt;

/// Export builtin
///
/// Only one variable can be set at a time.
///
/// # Example:
/// `export Variable arg1 arg2 arg3 $OTHER_VAR`
///
/// This example will create a variable named `Variable` with values arg1, arg2,
/// arg3 and the contents of `$OTHER_VAR`.
///
/// # Arguments
/// * `args` => The first arg will always be the key and the remainder the values.
pub fn export(args: &[String]) -> Result<(), Error> {
    if !args.is_empty() {
        for arg in args {
            if arg.contains('=') || arg.contains('\0') {
                return Err(Error::IllegalChars);
            }
        }

        let key = args[0].to_owned();
        let mut values = String::new();

        for (i, value) in args[1..].to_owned().iter().enumerate() {
            if i == 0 {
                values = value.to_owned();
            } else {
                values = format!("{}:{}", values, value);
            }
        }

        env::set_var(key, values);
    }

    Ok(())
}

/// export builtin Error type
#[derive(Debug)]
pub enum Error {
    /// The environment variable name or its value contain ilegal charaters
    IllegalChars,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::IllegalChars => write!(
                f,
                "the value or key can't contain an equal character or \
                 null character"
            ),
        }
    }
}
