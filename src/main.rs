#[macro_use]
extern crate log;
extern crate env_logger;
extern crate hs;
extern crate libc;
extern crate some_sort_of_prompt;

use std::env;
use std::path::PathBuf;
use std::process;

use some_sort_of_prompt::Prompt;

use hs::instr::instruction::Instruction;
use hs::jobs::job_handler::JobHandler;

fn main() {
    // Create Log
    env_logger::init().unwrap();

    // Create Job Handler
    let mut job_handler = JobHandler::new();

    // Ignore SIGNINT and SIGSTOP
    unsafe {
        let _ = libc::signal(libc::SIGINT, libc::SIG_IGN);
        let _ = libc::signal(libc::SIGTSTP, libc::SIG_IGN);
        let _ = libc::signal(libc::SIGTTOU, libc::SIG_IGN);
    }

    // Set foreground process to the shell
    unsafe {
        let _ = libc::tcsetpgrp(0, libc::getpid());
    }

    loop {
        // Handle Jobs
        job_handler
            .handle_jobs()
            .unwrap_or_else(|err| eprintln!("hs handle jobs error: {}", err));

        // Get current directory
        let current_dir = match env::current_dir() {
            Ok(path) => path,
            Err(err) => {
                eprintln!("hs error: getting current directory: {}", err);
                PathBuf::new()
            }
        };

        let mut prompt = Prompt::new(format!("hs${}> ", current_dir.display()));

        // Print Prompt
        print!("{}", prompt);

        // Read input, parse and execute
        let line = prompt.read();
        if line.len() > 1 {
            match line.parse::<Instruction>() {
                Ok(instruction) => {
                    debug!("Command Struct: {:#?}", instruction);
                    // Store instruction in job handler
                    job_handler.add_job(instruction);
                    // Spawn instruction in job handler
                    job_handler
                        .spawn_job()
                        .unwrap_or_else(|err| eprintln!("hs error: {}", err));
                }
                Err(err) => eprintln!("hs error: parse error: {}", err),
            };
        } else if line.len() == 0 {
            // Exit on EOF
            break;
        }
    }

    job_handler.quit_all_jobs();
    process::exit(0);
}
