//! Error Type for the Human Shell
use std::fmt;
use std::result;

/// Error Type for the human shell shell
#[derive(Debug)]
pub enum Error {
    /// Message of the error
    Message(String),
}

/// Custom Result type
pub type Result<T> = result::Result<T, Error>;

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Message(msg) => write!(f, "{}", msg),
        }
    }
}

impl From<String> for Error {
    fn from(str: String) -> Self {
        Error::Message(str)
    }
}

impl From<&'static str> for Error {
    fn from(str: &str) -> Self {
        Error::Message(str.to_owned())
    }
}
