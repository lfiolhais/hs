//! Signal Handling module
use nix::sys::signal;
use nix::sys::signal::Signal;
use nix::Error as NixError;

/// Send a signal to a specific PID
///
/// # Arguments
/// * `pid` => PID of process to kill
/// * `signal` => Signal to send
///
/// # Return Value
/// Nothing on success and error message on error
pub fn kill(pid: i32, signal: Signal) -> Result<(), NixError> {
    match signal::kill(pid, signal) {
        Ok(_) => Ok(()),
        Err(err) => Err(err),
    }
}
