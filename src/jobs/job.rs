//! Job Module
use crate::error::Result as HsResult;
use crate::instr::instruction::{Instruction, WaitResult};

use nix::sys::signal::Signal;

use std::fmt;

/// Job data struct
#[derive(Debug)]
pub struct Job {
    id: usize,
    instruction: Instruction,
    status: JobStatus,
}

impl Job {
    /// Create a Job object
    ///
    /// # Arguments
    /// * `id` => job id
    /// * `instruction` => instruction for job to execute
    ///
    /// # Return Value
    /// Job object
    pub fn new(id: usize, instruction: Instruction, status: JobStatus) -> Self {
        Job {
            id,
            instruction,
            status,
        }
    }

    /// Obtain job id
    pub fn get_id(&self) -> usize {
        self.id
    }

    /// Immutably Borrow Job Instruction
    pub fn borrow_instruction(&self) -> &Instruction {
        &self.instruction
    }

    /// Mutably Borrow Job Instruction
    pub fn borrow_instruction_mut(&mut self) -> &mut Instruction {
        &mut self.instruction
    }

    /// Change Job status
    pub fn change_job_status(&mut self, new_status: JobStatus) {
        self.status = new_status;
    }

    /// Check if there are any children to collect in this job
    pub fn has_children(&self) -> bool {
        self.instruction.has_children()
    }

    /// Check if the instruction in the Job is a sole Builtin with no `no_wait` operator, pipeline
    /// or redirection.
    pub fn is_sole_builtin(&self) -> bool {
        self.instruction.n_commands() == 1
            && !self.instruction.get_no_wait()
            && !self.instruction.has_children()
            && !self.instruction.has_redirection()
            && !self.instruction.has_pipeline()
    }

    /// Execute job instruction
    pub fn spawn_instruction(&mut self, jobs: &mut [Job]) -> HsResult<()> {
        self.instruction.spawn_instruction(jobs)
    }

    /// Try to collect children and remove them from the children list.
    ///
    /// # Return Value
    /// OUTDATED: Is true if all children from a job were collected, false otherwise
    pub fn collect_children(&mut self) -> HsResult<WaitResult> {
        self.instruction.wait_on_children()
    }

    /// Send job to the background
    pub fn send_bg(&mut self) {
        self.change_job_status(JobStatus::Stopped);
        self.instruction.set_no_wait(true);

        // Set shell as foreground process
        unsafe {
            let _ = libc::tcsetpgrp(0, libc::getpid());
        }
    }

    /// SIGTERM all children in the job
    pub fn kill_all(&mut self) {
        // Need to continue stopped processes before we literally kill them
        self.instruction
            .kill_children(Signal::SIGCONT)
            .unwrap_or_else(|err| eprintln!("{}", err));

        self.instruction
            .kill_children(Signal::SIGTERM)
            .unwrap_or_else(|err| eprintln!("{}", err));

        match self.instruction.wait_on_children() {
            Ok(_) => {}
            Err(err) => eprintln!("{}", err),
        }
    }
}

impl fmt::Display for Job {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:<6}\t ", self.id,)?;

        if self.instruction.get_raw_txt().len() > 20 {
            let mut new_instr = self.instruction.get_raw_txt().to_owned();
            new_instr.truncate(17);
            write!(f, "{:<17}...\t ", new_instr)?;
        } else {
            write!(f, "{:<20}\t ", self.instruction.get_raw_txt())?;
        }

        write!(f, "{:<9}\t ", self.status)?;

        if !self.instruction.get_children().is_empty() {
            write!(f, "{:<9}", self.instruction.get_children()[0])?;
        } else {
            write!(f, "{:<9}", "-")?;
        }

        Ok(())
    }
}

/// The status of a job in the handler
#[derive(Debug, Clone, Copy)]
pub enum JobStatus {
    /// Job is currently running
    Running,
    /// Job stopped
    Stopped,
    /// Job has yet to be spawned. Realistically, a user should never see this state.
    Unspawned,
}

impl fmt::Display for JobStatus {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            JobStatus::Running => write!(f, "Running"),
            JobStatus::Stopped => write!(f, "Stopped"),
            JobStatus::Unspawned => write!(f, "Unspawned"),
        }
    }
}
