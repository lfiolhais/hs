//! Job Handler Module
//! Handle jobs given by the shell. This module keeps a list of jobs to execute
//! and wait, as well as the id for each job.
use super::job::{Job, JobStatus};
use crate::error::Result as HsResult;
use crate::instr::instruction::Instruction;
use crate::instr::instruction::WaitResult;

/// Struct `JobHandler`
#[derive(Debug, Default)]
pub struct JobHandler {
    last_job_id: usize,
    jobs: Vec<Job>,
}

impl JobHandler {
    /// Create JobHandler struct
    pub fn new() -> Self {
        JobHandler {
            last_job_id: 0,
            jobs: Vec::new(),
        }
    }

    /// Add job and update next `last_job_id`
    ///
    /// # Arguments
    /// * `instr` => Instruction to execute
    pub fn add_job(&mut self, instr: Instruction) {
        // Create Job with last job id
        let job = Job::new(self.last_job_id, instr, JobStatus::Unspawned);

        debug!("New Job: {:#?}", job);

        // Add Job into queue
        self.jobs.push(job);

        debug!("Jobs: {:#?}", self.jobs);

        // Update last job id to next free job id. Get the next job which isn't
        // currently used.
        // There wasn't a job remove prior to this add. Next job id may or
        // may not be the last plus 1. It should only be self.last_job_id +
        // 1 if we are certain the next job id isn't being used by another
        // Job.
        let mut found_id: bool = true;
        let mut tmp_last_job_id = self.last_job_id + 1;

        while found_id {
            found_id = self.does_job_id_exist(tmp_last_job_id);
            if found_id {
                tmp_last_job_id += 1;
            }
        }

        self.last_job_id = tmp_last_job_id;
        debug!("New Job ID: {:#?}", self.last_job_id);
    }

    /// Get all jobs in the job handler
    pub fn get_jobs(&self) -> &[Job] {
        &self.jobs
    }

    /// Spawn most recent job
    pub fn spawn_job(&mut self) -> HsResult<()> {
        let most_recent_job = self.jobs.len() - 1;
        let (mut all_other_jobs, latest_job) = self.jobs.split_at_mut(most_recent_job);

        match latest_job[0].spawn_instruction(&mut all_other_jobs) {
            Ok(_) => {
                latest_job[0].change_job_status(JobStatus::Running);
                Ok(())
            }
            Err(err) => Err(err),
        }
    }

    /// Remove job at index `job_id` and update `last_job_id`
    ///
    /// Currently this functions does not check if the id passed exists in the
    /// jobs vector.
    ///
    /// # Arguments
    /// * `job_id` => job id to remove
    fn rm_job(&mut self, job_id: usize) {
        let job_index = self.get_index_from_job_id(job_id).unwrap();
        println!(
            "Done: Job {} => {}",
            self.jobs[job_index].get_id(),
            self.jobs[job_index].borrow_instruction().get_raw_txt()
        );
        debug!("Removed {:#?}", self.jobs[job_index]);

        // Remove job
        self.jobs.retain(|job| job.get_id() != job_id);

        debug!("New Jobs {:#?}", self.jobs);

        // Update last id if current job id is smaller
        if job_id < self.last_job_id {
            self.last_job_id = job_id;
        }
    }

    fn get_index_from_job_id(&self, job_id: usize) -> Option<usize> {
        for (i, job) in self.jobs.iter().enumerate() {
            if job.get_id() == job_id {
                return Some(i);
            }
        }
        None
    }

    /// Send a given job to background
    fn send_bg(&mut self, job_id: usize) {
        self.jobs[job_id].send_bg();
    }

    /// Performing Handling commands on jobs currently active
    pub fn handle_jobs(&mut self) -> HsResult<()> {
        let jobs_to_handle: Vec<(HsResult<WaitResult>, usize)> = self
            .jobs
            .iter_mut()
            .enumerate()
            .map(|(id, job)| {
                // If the job is just a Builtin with no no_wait operator we can safely remove
                // without collecting its children.
                if !job.is_sole_builtin() {
                    (job.collect_children(), id)
                } else {
                    (Ok(WaitResult::AllDone), id)
                }
            })
            .collect();

        // Handle dead jobs
        jobs_to_handle
            .iter()
            .filter(|(result, _)| result.as_ref().ok() == Some(&WaitResult::AllDone))
            .for_each(|(_, id)| self.rm_job(*id));

        // Handle specific commands to the job handler
        jobs_to_handle
            .iter()
            .filter(|(result, _)| {
                result.as_ref().ok()
                    == Some(&WaitResult::JobHandlerAction(JobHandlerActions::SendJobBg))
            })
            .for_each(|(_, id)| self.send_bg(*id));

        // Set foreground process to the shell
        unsafe {
            let _ = libc::tcsetpgrp(0, libc::getpid());
        }

        Ok(())
    }

    /// Check for the existence of `job_id`
    ///
    /// # Arguments
    /// * `job_id` => job id
    ///
    /// # Return Value
    /// True if it exists false otherwise
    fn does_job_id_exist(&self, job_id: usize) -> bool {
        for job in &self.jobs {
            if job_id == job.get_id() {
                return true;
            }
        }

        false
    }

    /// Quit all jobs in the background
    pub fn quit_all_jobs(&mut self) {
        self.jobs.iter_mut().for_each(|job| job.kill_all());
    }
}

/// Actions for the JobHandler to perform
#[derive(Debug, PartialEq, Eq)]
pub enum JobHandlerActions {
    /// Send job to background
    SendJobBg,
}
