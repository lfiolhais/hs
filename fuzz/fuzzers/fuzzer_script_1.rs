#![no_main]
#[macro_use]
extern crate libfuzzer_sys;
extern crate hs;

use hs::instr::instruction::Instruction;
use std::str;

fuzz_target!(|data: &[u8]| {
    // fuzzed code goes here
    if let Ok(s) = str::from_utf8(data) {
        println!();
        println!("START");
        println!("Input: {:#?}", s);
        match s.parse::<Instruction>() {
            Ok(instr) => {
                println!("{:#?}", instr);
                // Close fds
                for cmd in instr.commands {
                    cmd.close_all_fds();
                }
            }
            Err(err) => println!("Error: {}", err),
        }
        println!("END");
        println!();
    }
});
