# Human Shell
A shell in Rust with a focus on user friendliness. The project is in its early
stages.

# Goals
The goal of the project is to write a shell that is user friendly using as
little dependencies as possible in Rust. The only dependencies exceptions
are [`nix`](https://github.com/nix-rust/nix) and, at some
point, [`termion`](https://github.com/ticki/termion). Ideally `nix` wouldn't be
necessary but the current API's in Rust for forking processes, creating
anonymous pipes and signal handling are underdeveloped or non-existent.

I don't intend to support Windows (sorry :().

# Features

## Completed
- [x] Execute external commands
- [x] Pipes
- [x] `>`, `>>`, `<` redirections
- [x] `cd`, `echo`, `export`, `exit` builtin
- [x] env variable expansion
- [x] signal handling
- [x] `&` operator

## Not Completed
- [ ] `2>&1` operators
- [ ] autocomplete

# Build and Run
To download and install Rust, go [here](https://www.rust-lang.org)
and then just `cargo run`.

# License
Licensed under GPL 3.0 ([LICENSE](LICENSE)
or
[https://www.gnu.org/licenses/gpl-3.0.txt](https://www.gnu.org/licenses/gpl-3.0.txt)).
